# Configuration File Reader In Python

A simple module that reads your custom .conf files. See requirements below
# Usage
This is not meant to be ran on its own.  Import it into your application using
```py
import ConfigReader
```
You can also use one of the two classes ``NumsOnlyFile`` and ``AnyFile``
```py
from ConfigReader import NumsOnlyFile
from ConfigReader import AnyFile
```
## NumsOnlyFile
Accepts only integer/float values.
## AnyFile
Accepts integer/float/string values
# Requirements
Your file must follow these rules:
- Must use ``variable=value`` 
- Must only use commas, not spaces or parenthesis for tuples.
- The output is a dictionary. You have to use the method .get_variables() first.
## Example Config File
*filename.conf*
```
RGB=93,70,147
pi=3.14
noerror=3.
test = 21
hello    =hello world
random characters,=a1.21
string?='21'
```
# Usage
To use this in your code, simply call either of the two classes shown above.
```py
sample = NumsOnlyFile("filename.conf")
```
if the file is in a different directory, you can specify that. By default it searches in your current working directory.
```py
sample = AnyFile("filename.conf","/config/")
```
To get the variables, use the ``get_variables`` method
```py
dictionaryVariables = sample.get_variables()
```
# Example
 ```py
 from ConfigReader import AnyFile
 sample = AnyFile("filename.conf")
 variables = sample.get_variables()
 print(variables)
 ```
 ```
 output:
{
'RGB': (93,70,147),
'pi': 3.14, 
'noerror': 3.0, 
'test': 21, 
'hello': 'hello world', 
'randomcharacters,': 'a1.21', 
'string?': "'21'"
}
```
