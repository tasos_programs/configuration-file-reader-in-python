from dataclasses import dataclass
from typing import Optional
from abc import ABC,abstractmethod
from ast import literal_eval as make_tuple
import os

@dataclass
class File(ABC):
	filename :str
	location : Optional[str] = os.getcwd()
    
	def open_file(self):
		# Do a basic change if running on windows.
		if os.name == "nt":
			file = self.location+"\\"+self.filename
		else:
			file = self.location+"/"+self.filename
		return open(file,"r")

	def read_lines(self) -> list:
		file = self.open_file()
		return file.readlines()
		
	def split_line(self,line) -> list:
		return line.split("=")
		
	def int_or_float(self,number) -> int or float:
		return float(number) if "." in number else int(number)
	
	def is_int(self,value) -> bool:
		try:
			float(value) # if we int(value) it makes 'value' an int 
			return True
		except ValueError:
			return False
	
	def check_if_missing_variable(self,var) -> bool:
		# Since we might not even get an =, we need to use try
		try:
			value = var[1]
		except IndexError:
			print("Syntax Error. No '=' for variable:", var[0])
			return True
		if value == "":
			print ("Missing value for variable:", var[0])
			return True
		elif ("(" in value or ')' in value): # this if statement could cause issues if var is string
			print ("Parenthesis can't be parsed at the moment. Please remove them.")
			return True
		return False
	
	def remove_whitespace_if_string(self,split : str) -> list:
		# remove whitespace in variable either way
		# remove whitespace in value IF it's an int.
		variable = split[0].replace(' ','')
		value = split[1].replace(' ','') if self.is_int(split[1]) else split[1]

		return [variable,value]

	def is_tuple(self,value) -> bool:
		if "," in value:
			print(", in ",value)
			# since we can just put a value like "h3ll0, world" meaning a string, check for string chars too
			for character in value:
				if character == ",": continue
				if not self.is_int(character): return False # String char found, return false								
			return True
		# No tuples found
		return False

	def get_tuple(self,value) -> tuple:
		value = "(" + value + ")"
		return make_tuple(value)

	def do_basic_checks(self,line : str) -> list or None:
		split = [a.rstrip("\n") for a in self.split_line(line)] #split on '='' and remove newline string
		# We also need to remove \n for variable name AND variable if not a string.
		if self.check_if_missing_variable(split):
			return None
		split = self.remove_whitespace_if_string(split)
		return split
	

	@abstractmethod		
	def get_variables(self,lines) -> dict:
		"""Gets variables from config file"""
		
class AnyFile(File):
	"""Represents a config file that allows for string values in variables"""	
	def get_variables(self) -> dict:
		lines = self.read_lines()
		dvars ={}
		for line in lines:
			splitLines = self.do_basic_checks(line)
			if splitLines == None:
				print("Error parsing variable")
				continue
			value = splitLines[1]
			"""Append an int/float/string/tuple depending on type"""
			dvars[splitLines[0]] = self.int_or_float(value) if self.is_int(value) else self.get_tuple(value) if self.is_tuple(value) else value
		return dvars

class NumsOnlyFile(File):
	"""Represents a config file that does not allow for string values in variables"""
	def get_variables(self) -> dict:
		lines = self.read_lines()
		dvars ={}
		for line in lines:
			splitLines = self.do_basic_checks(line)
			if splitLines == None:
				print("Error parsing variable")
				continue		
			value = splitLines[1]			
			"""Append an int/float/tuple but not a string, depending on type."""
			if self.is_int(value):
				dvars[splitLines[0]] = self.int_or_float(value)
			elif self.is_tuple(value):
				dvars[splitLines[0]] = self.get_tuple(value)
		return dvars
